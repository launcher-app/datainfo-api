<?php

namespace Hallboav\DatainfoApi\Exception;

/**
 * @author Hallison Boaventura <hallisonboaventura@gmail.com>
 */
class InvalidCredentialsException extends \RuntimeException
{
}
